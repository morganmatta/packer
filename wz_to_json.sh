#!/usr/bin/env bash

rm -f packer_cs_id

docker build -t packer_cs packer_cs
docker run --cidfile packer_cs_id packer_cs

PACKER_CS_CONTAINER_ID=`cat packer_cs_id`

docker cp $PACKER_CS_CONTAINER_ID:/packer/dump .

docker rm -f $PACKER_CS_CONTAINER_ID
rm -f packer_cs_id
