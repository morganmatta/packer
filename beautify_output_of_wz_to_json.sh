#!/usr/bin/env bash
rm -f packer_js_id

docker build -t packer_js packer_js
docker run --cidfile packer_js_id -v `pwd`/dump:/packer/dump packer_js node beautify.js

PACKER_JS_CONTAINER_ID=`cat packer_js_id`

docker cp $PACKER_JS_CONTAINER_ID:/packer/beautified .

docker rm -f $PACKER_JS_CONTAINER_ID
rm -f packer_js_id
