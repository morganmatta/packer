#!/usr/bin/env bash

docker build -t packer_js packer_js
docker run --cidfile packer_js_id -v `pwd`/dump:/packer/dump -v `pwd`/beautified:/packer/beautified packer_js node validate.js dump beautified

PACKER_JS_CONTAINER_ID=`cat packer_js_id`

docker rm -f $PACKER_JS_CONTAINER_ID
rm -f packer_js_id
