#!/usr/bin/env bash

rm -f packer_js_id

docker build -t packer_js packer_js
docker run --cidfile packer_js_id -v `pwd`/dump:/packer/wz packer_js node --max-old-space-size=8192 json_to_bin.js

PACKER_JS_CONTAINER_ID=`cat packer_js_id`

docker cp $PACKER_JS_CONTAINER_ID:/packer/binWZ .

docker rm -f $PACKER_JS_CONTAINER_ID
rm -f packer_js_id
