'use strict';

const fs = require('fs');
const path = require('path');

class WZReader {
  constructor(buf) {
    this.buf = buf;
    this.offset = 0;
  }
  read(count) {
    const ret = this.buf.slice(this.offset, this.offset + count);
    this.offset += count;
    return ret;
  }
  readInt8() {
    return this.read(1).readInt8(0);
  }
  readUInt8() {
    return this.read(1).readUInt8(0);
  }
  readCompressedInt16() {
    const oneByte = this.readInt8();
    if (oneByte !== -0x80) {
      return oneByte;
    }
    return this.read(2).readInt16BE(0);
  }
  readCompressedUInt16() {
    const oneByte = this.readUInt8();
    if (oneByte !== 0xff) {
      return oneByte;
    }
    return this.read(2).readUInt16BE(0);
  }
  readCompressedInt32() {
    const oneByte = this.readInt8();
    if (oneByte === -0x80) {
      return this.read(4).readInt32BE(0);
    } else if (oneByte === -0x7f) {
      return this.read(2).readInt16BE(0);
    }
    return oneByte;
  }
  readCompressedUInt32() {
    const oneByte = this.readUInt8();
    if (oneByte === 0xff) {
      return this.read(4).readUInt32BE(0);
    } else if (oneByte === 0xfe) {
      return this.read(2).readUInt16BE(0);
    }
    return oneByte;
  }
  readStringOrDuplicate() {
    const encoding = 'UTF-8';
    const oneByte = this.readInt8();
    if (oneByte === 0x01) {
      const stringOffset = this.read(4).readUInt32BE(0);
      const currentOffset = this.offset;
      this.offset = stringOffset;
      const duplicateNumBytes = this.readCompressedUInt16();
      const duplicate = this.read(duplicateNumBytes).toString(encoding);
      this.offset = currentOffset;
      return duplicate;
    }
    const numBytes = this.readCompressedUInt16();
    return this.read(numBytes).toString(encoding);
  }
  readCanvasOrDuplicate() {
    const encoding = 'base64';
    const prefix = 'iVBORw0KGgoAAAANSUhEUgAA';
    const oneByte = this.readInt8();
    if (oneByte === 0x01) {
      const canvasOffset = this.read(4).readUInt32BE(0);
      const currentOffset = this.offset;
      this.offset = canvasOffset;
      const duplicateWidth = this.readCompressedUInt16();
      const duplicateHeight = this.readCompressedUInt16();
      const duplicateNumBytes = this.readCompressedUInt32();
      const duplicateData = this.read(duplicateNumBytes).toString(encoding);
      this.offset = currentOffset;
      return {
        w: duplicateWidth,
        h: duplicateHeight,
        b: `${prefix}${duplicateData}`,
      };
    }
    const width = this.readCompressedUInt16();
    const height = this.readCompressedUInt16();
    const numBytes = this.readCompressedUInt32();
    const data = this.read(numBytes).toString(encoding);
    return {
      w: width,
      h: height,
      b: `${prefix}${data}`,
    };
  }
  readAudioOrDuplicate() {
    const encoding = 'base64';
    const oneByte = this.readInt8();
    if (oneByte === 0x01) {
      const audioOffset = this.read(4).readUInt32BE(0);
      const currentOffset = this.offset;
      this.offset = audioOffset;
      const duplicateNumBytes = this.readCompressedUInt32();
      const duplicate = this.read(duplicateNumBytes).toString(encoding);
      this.offset = currentOffset;
      return duplicate;
    }
    const numBytes = this.readCompressedUInt32();
    const data = this.read(numBytes).toString(encoding);
    return data;
  }
  readFloat32() {
    return this.read(4).readFloatBE(0);
  }
  readFloat64() {
    return this.read(8).readDoubleBE(0);
  }
}

function deserialize(reader, level=0) {
  const n = reader.readStringOrDuplicate();
  const t = reader.readInt8();

  const ret = { n, t };

  if (t === 0) {
    if (level === 1) {
      ret.n = `${ret.n}.wz`;
    }
    const numChildren = reader.readCompressedUInt16();
    if (numChildren > 0) {
      const c = [];
      for (let i = 0; i < numChildren; i += 1) {
        c.push(deserialize(reader, level + 1));
      }
      ret.c = c;
    }
  } else if (t === 1) {
    ret.n = `${ret.n}.img`;
    const numChildren = reader.readCompressedUInt16();
    if (numChildren > 0) {
      const c = [];
      for (let i = 0; i < numChildren; i += 1) {
        c.push(deserialize(reader, level + 1));
      }
      ret.c = c;
    }
  } else if (t === 2) {
    const numChildren = reader.readCompressedUInt16();
    if (numChildren > 0) {
      const c = [];
      for (let i = 0; i < numChildren; i += 1) {
        c.push(deserialize(reader, level + 1));
      }
      ret.c = c;
    }
  } else if (t === 3) {
    const { w, h, b } = reader.readCanvasOrDuplicate();
    ret.w = w;
    ret.h = h;
    ret.b = b;
    const numChildren = reader.readCompressedUInt16();
    if (numChildren > 0) {
      const c = [];
      for (let i = 0; i < numChildren; i += 1) {
        c.push(deserialize(reader, level + 1));
      }
      ret.c = c;
    }
  } else if (t === 4) {
    const numChildren = reader.readCompressedUInt16();
    if (numChildren > 0) {
      const c = [];
      for (let i = 0; i < numChildren; i += 1) {
        c.push(deserialize(reader, level + 1));
      }
      ret.c = c;
    }
  } else if (t === 5) {
    ret.b = reader.readAudioOrDuplicate();
  } else if (t === 6) {
    ret.v = reader.readStringOrDuplicate();
  } else if (t === 7) {
    ret.v = reader.readCompressedInt32();
  } else if (t === 8) {
    ret.v = reader.readFloat64();
  } else if (t === 9) {
  } else if (t === 10) {
    ret.v = reader.readStringOrDuplicate();
  } else if (t === 11) {
    ret.v = reader.readCompressedInt16();
  } else if (t === 12) {
    ret.x = reader.readCompressedInt16();
    ret.y = reader.readCompressedInt16();
  } else if (t === 13) {
    ret.v = reader.readFloat32();
  } else {
    throw new Error(`Unknown type ${t}`);
  }

  return ret;
}

function dumpObj(obj, p) {
  if (obj.t === 0) {
    const parentDir = path.join(p, obj.n);
    fs.mkdirSync(parentDir);
    obj.c.forEach(child => dumpObj(child, parentDir));
  } else if (obj.t === 1) {
    fs.writeFileSync(path.join(p, `${obj.n}.json`), JSON.stringify(obj));
  }
}

const bytes = fs.readFileSync('binWZ');
console.log('Read serialized result');
const reader = new WZReader(bytes);
const obj2 = deserialize(reader);
console.log('Deserialized serialized result');

fs.mkdirSync('deserialized');
dumpObj(obj2, 'deserialized');
